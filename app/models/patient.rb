class Patient < ActiveRecord::Base
  has_many :appointments
  belongs_to :insurance
  has_many :specialist, through: :appointments
end