# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

specialists = Specialist.create ([name: 'Victor Mann', specialty: 'Cardiology'])
specialists = Specialist.create ([name: 'Gregory Smith', specialty: 'Surgeon'])
specialists = Specialist.create ([name: 'Randell Ruben', specialty: 'Dermatologist'])
specialists = Specialist.create ([name: 'Mark Blue', specialty: 'Cardiology'])
specialists = Specialist.create ([name: 'Smith Smithy', specialty: 'Dermatologist'])

patients = Patient.create ([name: 'Cecil McDonald', street_address: '1643 Calhoun Rd. Houston, TX 77004'])
patients = Patient.create ([name: 'Eti Erege', street_address: '65343 Wilby St. Houston, TX 77533'])
patients = Patient.create ([name: 'Zach Nguyen', street_address: '5621 Richmond Houston, TX 77098'])
patients = Patient.create ([name: 'Alexander Armstrong', street_address: '5347 Wheeler Houston, TX 77865'])
patients = Patient.create ([name: 'Patel Skimsa', street_address: '1212 Cage Rd. Houston, TX 77887'])

insurance = Insurance.create ([name: 'American Allied', street_address: '6549 Sage Rd. Houston, TX 77999'])
insurance = Insurance.create ([name: 'Group Alliant', street_address: '8747 Silber St. Houston, TX 77223'])
insurance = Insurance.create ([name: 'Health Co', street_address: '950 Treehouse Houston, TX 77863'])
insurance = Insurance.create ([name: 'Blue HMO', street_address: '2452 Green Park Houston, TX 77003'])
insurance = Insurance.create ([name: 'Baylor Medicine', street_address: '7337 Whispering Rd. Houston, TX 77004'])

